**POO i accés a dades**

M6UF1. Persistència en fitxers
==============================

Operacions sobre fitxers
------------------------

* [Interfície *Path*](path.md)

Lectura i escriptura de fitxers binaris
---------------------------------------

* [Lectura/escriptura byte a byte](fitxers_bytes.md)
* [Lectura/escriptura de dades binàries](fitxers_dades.md)
* [Lectura/escriptura d'objectes](fitxers_objectes.md)
* [Fitxers d'accés aleatori](acces_aleatori.md)

Lectura i escriptura de fitxers de text
---------------------------------------

* [Lectura/escriptura en format text](fitxers_text.md)
