## Lectura i escriptura de fitxers de text

Les classes bàsiques per llegir i escriure text en Java són
*FileReader* i *FileWriter* respectivament.

A diferència de les classes que hem vist fins ara, aquestes classes tracten
qualsevol tipus de dada primitiva com si fossin cadenes de text.

### Escriptura d'un fitxer de text
