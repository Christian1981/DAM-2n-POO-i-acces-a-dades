::Compilat o interpretat::El llenguatge Java és un llenguatge... {
  ~compilat. Cal compilar el codi per a cada plataforma en què s'hagi d'executar el programa.
  ~interpretat. No es compila mai el codi, perquè qui utilitzi el programa tindrà un intèrpret del codi font en Java.
  ~que utilitza una màquina virtual per a la interpretació directa del codi font Java, fent que aquest sigui multiplataforma.
  =que utilitza una màquina virtual per a la interpretació dels fitxers binaris creats pel compilador.
}

::Variables i objectes::[markdown]
En el disseny d'un programa detectem la necessitat de crear la classe *Gos*. Una de les propietats que ens interessarà dels gossos serà la seva *edat*. Quan ho implementem... {
  ~existirà una única variable "edat" que compartiran tots els objectes de tipus Gos.
  =existirà una variable "edat" per a cada objecte Gos que s'instanciï.
  ~existirà una operació "edat" que compartiran tots els objectes de tipus Gos.
  ~existiran tantes variables "edat" com atributs "edat" declarem al codi de la classe Gos.
}

::Propietats i operacions::Un objecte està format per propietats i operacions. {
  =Les propietats poden ser tipus simples o ve altres objectes.
  ~Les propietats han de ser sempre tipus simples.
  ~Les operacions poden ser tipus simples o ve altres objectes.
  ~Les operacions han de ser sempre tipus simples.
}

::Sinònim de propietat::Quin dels següents és un sinònim de les propietats d'un objecte: {
  ~herència
  =atributs
  ~mètodes
  ~permisos
}

::Instàncies::En la programació orientada a objectes... {
  =d'una classe en podem instanciar molts objectes.
  ~d'un objecte en podem instanciar moltes classes.
  ~de cada classe n'existirà un objecte.
  ~per a cada objecte que necessitem hem d'implementar una classe.
}

::Classes embolcall::[markdown]
Java proporciona classes embolcall per als tipus primitius. Per exemple, disposem de la classe *Integer* com a tipus embolcall de *int*. Quina de les següents afirmacions **NO** és correcte? {
  =Una variable de tipus Integer pot tenir exactament els mateixos valors que un int.
  ~El tipus Integer ens proporciona una sèrie de mètodes que permeten tractar amb nombres enters.
  ~Transformar un int en Integer o un Integer en int és immediat.
  ~El tipus Integer ens proporciona els valors màxim i mínim que pot emmagatzemar un int.
}

::Diverses classes en un sol fitxer::[markdown]
En un fitxer de codi font estem implementant les classes *ClassePrincipal* i *ClasseAuxiliar*. {
  ~Això no és possible, només es pot implementar una classe per fitxer.
  =Això és permès, però només una de les dues classes pot ser public.
  ~Això és permès, i les dues classes poden ser public.
  ~Això és permès, sempre i quan cap de les dues classes sigui public.
}

::Blocs de codi iniciador::Un bloc de codi iniciador... {
  ~en cap cas pot declarar-se com static.
  ~si es declara com static, s'executarà una vegada cada cop que es creï un objecte d'aquella classe.
  ~si es declara com static, només s'executarà una vegada, en el moment de crear el primer objecte de la classe.
  =si es declara com static, només s'executarà una vegada, en el moment en què es carregui la classe.
}

::Public i private::Habitualment, {
  =la major part de mètodes es declaren com public i la major part d'atributs com private.
  ~la major part de mètode es declaren com private i la major part d'atributs com public.
  ~la major part de mètodes i d'atributs es declaren com public.
  ~la major part de mètodes i d'atributs es declaren com private.
}

::Mètodes accessors::Per modificar un atribut d'un objecte s'utilitza habitualment un mètode accessor (set) perquè... {
  =això ens permet comprovar que la dada que es vol assignar a l'atribut sigui vàlida.
  ~el Java no permet que es modifiqui directament un atribut d'un objecte.
  ~no hi ha un motiu en especial, es fa per conveni.
  ~L'afirmació no és certa, habitualment els atributs es modifiquen directament.
}

::Quants objectes::[markdown]
Donat el codi següent\:\n
\n    String x \= new String("blau");
\n    String y \= new String("verd");
\n    String z \= x
\n\nQuants objectes hem creat? {
  ~0
  ~1
  =2
  ~3
}

::Igual o diferent::[markdown]
Indica què sortirà per pantalla després d'executar el codi següent\:\n
\n    Point p1, p2;
\n    p1 \= new Point(2, 3);
\n    p2 \= new Point(2, 3);
\n    if (p1 \=\= p2)
\n        System.out.println("Iguals");
\n    else
\n        System.out.println("Diferents");
\n
{
  ~Iguals
  =Diferents
}

::Mètodes sobrecarregats::[markdown]
Disposem del mètode *int cerca(double valor, int posicio)*. Quina de les següents declaracions **NO** és una sobrecàrrega vàlida: {
  ~int cerca(int posicio, double valor)
  ~int cerca(int valor, int posicio)
  ~int cerca(int valor)
  =double cerca(double valor, int posicio)
}

::Constructors::Les classes en Java... {
  =tenen totes almenys un constructor. Si el programador no n'implementa cap, el sistema en crea un per defecte.
  ~tenen totes un únic constructor, que el sistema crea per defecte si no volem personalitzar-lo.
  ~tenen totes múltiples constructors. El sistema en crea més o menys, depenent de la quantitat d'atributs de classe.
  ~poden no tenir cap constructor, encara que això és poc habitual.
}

::Constructors per defecte::El constructor per defecte d'una classe... {
  =no rep cap paràmetre.
  ~rep tants paràmetres com atributs tingui la classe.
  ~no és únic, n'hi ha més o menys en funció de quants atributs té la classe.
  ~sempre existeix.
}

::this::[markdown]
Quin dels següents usos **NO** és un ús vàlid de la paraula clau *this*? {
  ~Retornar en un mètode una referència al propi objecte.
  ~Utilitzar-lo com a prefix per accedir a un atribut de l'objecte.
  =Utilitzar-lo com a prefix per accedir a un atribut de la classe (és a dir, a un atribut static).
  ~Utilitzar-lo a la primera línia d'un constructor per cridar un altre constructor de la classe.
}

::Paquets i imports::[markdown]
Tenim la classe *ClasseA* al paquet *paquetA* i la *ClasseB* al paquet *paquetB*.
Des d'un mètode de la *ClasseB* volem crear un objecte de *ClasseA*.
Quina de les següents NO és una forma vàlida de fer-ho? {
  ~Al principi del fitxer ClasseB.java posem "import paquetA.ClasseA;". Fem referència a ClasseA directament.
  ~Al principi del fitxer ClasseB.java posem "import paquetA.*". Fem referència a ClasseA directament.
  =Al principi del fitxer ClasseA.java posem "import paquetB.*". Fem referència a ClasseA directament.
  ~Ens referim a la ClasseA com paquetA.ClasseA. No afegim sentència import.
}

::Fitxers jar::[markdown]
Quina de les següents afirmacions **NO** és certa? Un fitxer *.jar* ens permet... {
  ~**Tenir comprimit** un programa Java, que té diverses classes dins de diversos paquets, en un únic fitxer.
  =crear un fitxer executable natiu del sistema on ens trobem (per exemple, un .exe en Windows).
  ~executar directament el fitxer .jar a la màquina virtual de Java.
  ~importar directament totes les classes que hi ha al fitxer a un projecte del Netbeans o Eclipse.
}

::Conveni pel nom de les classes::En un programa necessitem representar-hi tigres de Bengala. Quin dels següents noms seria correcte per aquesta classe? {
  ~tigreDeBengala
  ~Tigre_de_Bengala
  =TigreDeBengala
  ~TigredeBengala
}

::Convenis i crides a mètodes::[markdown]
Si s'han seguit els estàndards de codificació de Java, què podem dir sobre la crida *String.valueOf(4);*? {
  =Que String és el nom d'una classe i que valueOf és un mètode static.
  ~Que String és el nom d'un objecte i que valueOf és un dels seus mètodes.
  ~Que String és un paquet i que valueOf és un objecte.
  ~Que String és el nom d'un objecte i que valueOf és un mètode static.
}
