##### Ús d'un iterador sense l'iterador: *enhanced for*

Es coneix amb el nom de **enhanced for** una variant del bucle *for* que
permet recórrer per tots els elements d'una col·lecció. Aquest és el
*for* per defecte que utilitzen molts llenguatges de scripting, com el
Bash o el Python.

En Java, aquest tipus de bucle s'escriu així:

```java
for (Integer i : c) {
    System.out.println(i);
}
```

on *c* és una col·lecció qualsevol que conté enters.

Aquest codi és equivalent a:

```java
Iterator<Integer> it = c.iterator();
while (it.hasNext())
    System.out.println(it.next());
```

L'*enhanced for* es va afegir al llenguatge Java a la seva versió
5, precisament per simplificar l'ús dels iteradors. És clar que no és
aplicable a totes les situacions, i que treballar amb els iteradors
segueix essent necessari, per exemple, si volem afegir elements durant
el recorregut, o alternar l'ordre del recorregut, però per a les
situacions més comunes, és una alternativa elegant.

***Exemple:***

Imaginem que tenim dues llistes, una amb els pals d'una baralla de
cartes, i l'altre amb els nombres possibles de les cartes. Volem crear
una tercera llista amb totes les cartes de la baralla, combinant cada
pal possible amb cada número possible.

Podríem intentar-ho així:

```java
List<Pal> pals = ...;
List<Numero> numeros = ...;
List<Carta> baralla = new ArrayList<Carta>();

// ERROR: llença NoSuchElementException!
for (Iterator<Pal> i = pals.iterator(); i.hasNext(); )
    for (Iterator<Numero> j = numeros.iterator(); j.hasNext(); )
        baralla.add(new Carta(i.next(), j.next()));
```

Quin és l'error en aquest codi?

Doncs que el mètode *next* per l'iterador *i* es crida moltes vegades
dins del bucle intern. Cada vegada ens retorna un pal diferent, i no
s'està comprovant quan s'acaben amb el *hasNext*.

Podríem arreglar-ho així:

```java
for (Iterator<Pal> i = pals.iterator(); i.hasNext(); ) {
    Pal pal = i.next();
    for (Iterator<Numero> j = numeros.iterator(); j.hasNext(); )
        baralla.add(new Carta(pal, j.next()));
}
```

Ara ens hem assegurat que el *next* d'*i* es crida un sol cop per
iteració, però el codi és una miqueta complicat de llegir. Amb un bucle
*enhanced for* la solució queda sorprenentment simple:

```java
for (Pal pal : pals)
    for (Numero num : numeros)
        baralla.add(new Carta(pal, num));
```

A més d'això, és important destacar que els bucles *enhanced for* també es
poden utilitzar amb els vectors estàtics de Java:

```java
// Retorna la suma dels elements d'a:
int suma(int[] a) {
    int resultat = 0;
    for (int i : a)
        resultat += i;
    return resultat;
}
```
