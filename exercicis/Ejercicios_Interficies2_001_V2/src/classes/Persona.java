package classes;

import java.util.Comparator;

public class Persona implements Comparable<Persona> {
	public float pes;
	public int edad;
	public float alcada;
	
	public static final Comparator<Persona> COMPARADOR_PES = (p1, p2) ->{
		if (p1.pes > p2.pes){
			return 1;
		}else if(p1.pes < p2.pes){
			return -1;
		}else{
			return 0;
		}
		
	};
	
	public Persona(float pes, int edad, float alt){
		super();
		this.pes = pes;
		this.edad = edad;
		this.alcada = alt;
	}

	@Override
	public int compareTo(Persona p) {
		
		if (this.edad > p.edad)
			return 1;
		else if (this.edad < p.edad)
			return -1;
		else
			return 0;
	}
}
