package test;

import classes.Persona;

public class Test {

	public static void main(String[] args) {
		Persona p1 = new Persona(89, 22, 1.80f);
		Persona p2 = new Persona(83, 42, 1.83f);
		
		
		if(p1.compareTo(p2) == 1){
			System.out.println("La persona p1 tiene: " + p1.edad + " es mayor que p2 que tiene: "+ p2.edad);
		}else if(p1.compareTo(p2) == -1){
			System.out.println("La persona p2 tiene: " + p2.edad + " es mayor que p1 que tiene: "+ p1.edad);
		}else{
			System.out.println("La persona p1 tiene la misma edad que la persona p2 y son "+ p1.edad);
		}
		
		if(Persona.COMPARADOR_PES.compare(p1, p2) == 1){
			System.out.println("La persona p1 pesa: " + p1.pes + " y pesa mas que p2 que pesa: "+ p2.pes);
		}else if(Persona.COMPARADOR_PES.compare(p1, p2) == -1){
			System.out.println("La persona p2 pesa: " + p2.pes + " y pesa mas que p1 que pesa: "+ p1.pes);
		}else{
			System.out.println("La persona p1 pesa lo mismo que la persona p2 y es: "+ p1.pes);
		}
		System.out.println("Resultado: " + p1.compareTo(p2));
		//p1.COMPARADOR_PES.compare(p1, p2);

	}

}
