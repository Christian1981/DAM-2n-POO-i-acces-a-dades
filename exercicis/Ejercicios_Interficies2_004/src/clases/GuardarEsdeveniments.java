package clases;

import java.util.ArrayList;

public class GuardarEsdeveniments implements Listener {
	
	public ArrayList<Integer> llista = new ArrayList<>();
	int esdeveniment;

	@Override
	public void notifyEvent(int n) {
		this.esdeveniment = n;
		llista.add(this.esdeveniment);
		//System.out.println(toString());
	}

	@Override
	public String toString() {
		String lis = new String();
		for (Integer i : llista){
			lis = lis + i + " ";
		}
		return lis;
	}
	
	
}
