package test;

import clases.Tirada;

public class Test {
	public static void main(String[] args) {
		Tirada t1, t2;
		//Tirada t1 = new Tirada(3, 2, 4);
		//Tirada t2 = new Tirada(2, 2, 3);
		
		//generem 100 partides
		for (int i = 0; i < 100; i++){
			t1 = new Tirada(3, 2, 4);
			t2 = new Tirada(2, 2, 3);
			System.out.println(t1.toString());
			System.out.println(t2.toString());
			System.out.println("Comparació de les dues tirades: " + t1.compareTo(t2));
		}
	}
}
